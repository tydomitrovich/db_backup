-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: imaging_metrics
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `imaging_metrics`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `imaging_metrics` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `imaging_metrics`;

--
-- Table structure for table `db_abuse`
--

DROP TABLE IF EXISTS `db_abuse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_abuse` (
  `entry_date` date NOT NULL,
  `sproc_count` int(11) NOT NULL,
  `environment` varchar(100) NOT NULL,
  `release_num` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_abuse`
--

LOCK TABLES `db_abuse` WRITE;
/*!40000 ALTER TABLE `db_abuse` DISABLE KEYS */;
INSERT INTO `db_abuse` VALUES ('2016-06-10',150,'dev-vm','4.1.2'),('2016-06-08',164,'dev-vm','4.1'),('2016-06-14',144,'dev-vm','4.1.3');
/*!40000 ALTER TABLE `db_abuse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imaging_mttf`
--

DROP TABLE IF EXISTS `imaging_mttf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imaging_mttf` (
  `event_date` date DEFAULT NULL,
  `environment` varchar(50) DEFAULT NULL,
  `event_description` varchar(200) DEFAULT NULL,
  `issue_number` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imaging_mttf`
--

LOCK TABLES `imaging_mttf` WRITE;
/*!40000 ALTER TABLE `imaging_mttf` DISABLE KEYS */;
INSERT INTO `imaging_mttf` VALUES ('2016-02-21','US','N/A','N/A'),('2016-02-26','US','N/A','N/A'),('2016-03-10','US','N/A','N/A'),('2016-04-01','US','N/A','N/A'),('2016-03-20','Paris','N/A','N/A'),('2016-02-27','Paris','N/A','N/A'),('2016-02-12','Invoice','N/A','N/A'),('2016-02-19','Invoice','N/A','N/A'),('2014-01-20','Paris','N/A','OPI-12887'),('2014-01-14','Paris','N/A','OPI-12466'),('2015-03-02','Invoice','N/A','OPI-52467'),('2016-04-16','Invoice','N/A','OPI-106754'),('2016-07-12','Paris','All imaging servers were accidentally taken offline instead of just one zone. Outage lasted Approx 20 mins. Related Jiras: IMG-2424','N/A'),('2015-05-21','US','N/A','RELEASE 4.0.1'),('2016-01-05','Paris','N/A','RELEASE 4.1'),('2016-01-22','US','N/A','RELEASE 4.1'),('2016-02-05','Paris','N/A','RELEASE 4.1.2'),('2016-02-12','US','N/A','RELEASE 4.1.2'),('2016-02-12','Invoice','N/A','RELEASE 4.1.2'),('2016-04-08','Invoice','N/A','RELEASE 4.1.3'),('2016-04-08','US','N/A','RELEASE 4.1.3'),('2016-04-01','Paris','N/A','RELEASE 4.1.3'),('2016-06-24','Paris','N/A','RELEASE 4.2'),('2016-06-24','US','N/A','RELEASE 4.2'),('2016-06-24','Invoice','N/A','RELEASE 4.2');
/*!40000 ALTER TABLE `imaging_mttf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imaging_redline`
--

DROP TABLE IF EXISTS `imaging_redline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imaging_redline` (
  `entry_date` date DEFAULT NULL,
  `redline_val` int(11) DEFAULT NULL,
  `release_num` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imaging_redline`
--

LOCK TABLES `imaging_redline` WRITE;
/*!40000 ALTER TABLE `imaging_redline` DISABLE KEYS */;
INSERT INTO `imaging_redline` VALUES ('2016-06-20',173,'4.1.3'),('2016-06-18',156,'4.1.2'),('2016-06-20',173,'4.1.3'),('2016-06-10',120,'4.0');
/*!40000 ALTER TABLE `imaging_redline` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18 17:25:44
